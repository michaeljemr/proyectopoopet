/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package petgame;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 *
 * @author Israel
 */
public class PetGame extends Application {
    
    public static Stage ventana;
    
    @Override
    public void start(Stage primaryStage) {
        
        this.ventana =  primaryStage;
        
        
        PrincipalPane principal = new PrincipalPane();
        Scene scene = new Scene(principal.getRoot(),300,250);
        
        primaryStage.setTitle("Pet Game");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}

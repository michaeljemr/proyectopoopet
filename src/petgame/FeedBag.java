/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package petgame;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import petgame.comida.Comida;

/**
 *
 * @author Israel
 */
public class FeedBag implements Runnable{
    public static ArrayList<Comida> misComidas = new ArrayList<>();
    private  Pane root = new Pane();
    private  int storage = misComidas.size();
    
    public void mostrarPrimero(){
    
        
        
        
        if(!misComidas.isEmpty()){
           if(!root.getChildren().contains(misComidas.get(0).getComidaView())){
                root.getChildren().add(misComidas.get(0).getComidaView());
                storage = misComidas.size();
                if(root.getChildren().get(0) instanceof Label){
        
                    root.getChildren().remove(root.getChildren().get(0));
            
                }
                
           }
        }
        else{
            root.getChildren().add(new Label("no hay alimento"));
        }
        System.out.println("aumento almacen: "+misComidas.size());
    }

    public Pane getRoot() {
        return root;
    }

    public void setRoot(Pane root) {
        this.root = root;
    }

    @Override
    public void run() {
       
        
            try {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        if(storage != misComidas.size()){
                            mostrarPrimero();
                        }
                    }
                });
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(FeedBag.class.getName()).log(Level.SEVERE, null, ex);
            }
        
    }
    
    public void actualizar(){
    
        Thread hilo = new Thread(this);
        hilo.setDaemon(true);
        hilo.run();
    }
    
    
}

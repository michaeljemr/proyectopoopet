/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package petgame;

import java.util.Optional;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.StageStyle;

/**
 *
 * @author Israel
 */
public class PrincipalPane {
    
    private Pane root;
    private Button jugar;
    
    public PrincipalPane(){
          this.root = new Pane();
          this.jugar = new Button("Jugar");
          this.jugar.setLayoutX(250/2);
          this.jugar.setLayoutY(300/2);
          this.root.getChildren().addAll(this.jugar);
          this.jugar.setOnAction(e->this.iniciarJuego());
    }

    public Pane getRoot() {
        return root;
    }

    public void setRoot(Pane root) {
        this.root = root;
    }

    public Button getJugar() {
        return jugar;
    }

    public void setJugar(Button jugar) {
        this.jugar = jugar;
    }
    
    
    public void iniciarJuego(){
        System.out.println("se inicia el juego");
        
        // se ingresa el nombre del usuario
        TextInputDialog dialogo = new TextInputDialog();
        dialogo.setTitle("Datos de usuario");
        dialogo.setHeaderText("Ingrese su nombre ");
        dialogo.setContentText("Por Favor escriba su nombre");
        dialogo.initStyle(StageStyle.UTILITY);
        Optional<String> respuesta = dialogo.showAndWait();
        System.out.println(respuesta.get());
        
        // se ingresa el nombre de la mascota
        
        TextInputDialog dialogo2 = new TextInputDialog();
        dialogo2.setTitle("Datos de la mascota");
        dialogo2.setHeaderText("Ingrese el nombre dela mascota");
        dialogo2.setContentText("Por Favor escriba el nombre");
        dialogo2.initStyle(StageStyle.UTILITY);
        Optional<String> respuesta2 = dialogo2.showAndWait();
        Pet.nombre = respuesta2.get();
        System.out.println(Pet.nombre);
        
        
        
        PetGame.ventana.setScene(new Scene(new PetPane().getRoot(),300,250));
        PetGame.ventana.show();
        
    }
    
}

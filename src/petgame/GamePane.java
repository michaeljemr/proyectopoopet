/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package petgame;

import java.awt.MouseInfo;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;

/**
 *
 * @author Israel
 */
public class GamePane {
    
    private BorderPane root ;
    private Button center ;
    private Arco arco = new Arco();
     public GamePane(){
     
         this.root = new BorderPane();
         this.center = new Button();
         this.center.setGraphic(arco.getArcoImageView());
         this.root.setOnMouseMoved(e->this.rotateActionArch());
         this.root.setCenter(center);
     
     }
    
    public  void rotateActionArch(){
    
        this.center.setRotate(Math.toDegrees(Math.cos(MouseInfo.getPointerInfo().getLocation().x)));
        
        
    
    } 
     
     
    public BorderPane getRoot(){
        return this.root;
    }
}

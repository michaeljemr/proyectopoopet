/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package petgame.comida;

import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;

/**
 *
 * @author Israel
 */
public class Comida {
    private int precio;
    private int cantidadAlim;
    private ImageView ComidaView;
    private String name;

    public Comida(int precio, int cantidadAlim,String path) {
        String str = path;
        String [] strings = str.split("\\.");
        this.name = strings[0];
        System.out.println(strings[0]);
        this.precio = precio;
        this.cantidadAlim = cantidadAlim;
        this.ComidaView = new ImageView(new Image(this.getClass().getResourceAsStream(path)));
        this.ComidaView.setFitHeight(30);
        this.ComidaView.setFitWidth(30);
       
    }
    public Comida(Comida comida){
    
        this.precio = comida.getPrecio();
        this.cantidadAlim = comida.getCantidadAlim();
        this.ComidaView = comida.getComidaView();
        this.name = comida.getName();
        
    }
    
   

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public int getCantidadAlim() {
        return cantidadAlim;
    }

    public void setCantidadAlim(int cantidadAlim) {
        this.cantidadAlim = cantidadAlim;
    }

    public ImageView getComidaView() {
        return ComidaView;
    }

    public void setComidaView(ImageView ComidaView) {
        this.ComidaView = ComidaView;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    
    
}

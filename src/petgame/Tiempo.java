/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package petgame;

import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.scene.control.Label;

/**
 *
 * @author Israel
 */
public class Tiempo implements Runnable{
    private Label tiempoView;
    private String segundo;
    
    public Tiempo(Label tiempoView){
       this.tiempoView = tiempoView;
       
    
    }

    public Label getTiempoView() {
        return tiempoView;
    }

    public void setTiempoView(Label tiempoView) {
        this.tiempoView = tiempoView;
    }

    public String getSegundo() {
        return segundo;
    }

    public void setSegundo(String segundo) {
        this.segundo = segundo;
    }
    
    
    
    
    
    

    @Override
    public void run() {
       
        while(true){
            try {
                int cont = Integer.parseInt(tiempoView.getText());
                
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        tiempoView.setText(String.valueOf(cont+1));
                        
                    }
                });
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(Tiempo.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    
    public void iniciar(){
    
        Thread hiloTiempo = new Thread(this);
        hiloTiempo.setDaemon(true);
        hiloTiempo.start();
        ;
    }
}

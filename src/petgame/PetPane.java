/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package petgame;

import java.awt.MouseInfo;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.ImageCursor;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.Clipboard;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import petgame.comida.Comida;
import petgame.comida.Tienda;

/**
 *
 * @author Israel
 */
public class PetPane {
    
    public static  BorderPane root;
    public static Pane center;
    private HBox top;
    private VBox right;
    private VBox left;
    private Button miniJuego;
    private Pet pet;
    public static Label coin;
    private Label tiempo;
    private Button alimentar;
    private ImageView cursorImage;
    public static FeedBag almacen;
    
    
    public PetPane(){
    
        
        pet = new Pet();
        tiempo = new Label("0");
        Tiempo time = new Tiempo(tiempo);
        coin = new Label();
        pet.petMovement();
        this.cursorImage = new ImageView(new Image("Images/PANTALLA PRINCIPAL.PNG"));
        this.cursorImage.setFitHeight(10);
        this.cursorImage.setFitWidth(10);
        almacen = new FeedBag();
        almacen.mostrarPrimero();
        
        center = new Pane();
        this.root = new BorderPane();
        this.top = new HBox();
        this.right = new VBox();
        this.left = new VBox();
        
        Button irTienda = new Button("Tienda");
        this.miniJuego = new Button("Minijuego");
        this.alimentar = new Button("Alimentar");
        
        this.miniJuego.setOnAction(e->this.jugar());
        irTienda.setOnAction(e->this.irTienda());
        this.alimentar.setOnAction(e->this.alimentar());
       
        this.coin.setText(String.valueOf(pet.getDinero()));
        
        this.root.setTop(this.top);
        this.root.setRight(this.right);
        this.root.setLeft(this.left);
        this.root.setCenter(center);
        
        this.top.getChildren().addAll(pet.getLife().getVidaLabel(),tiempo);
        this.left.getChildren().addAll(pet.getAlimentacionLabel(),this.pet.getAnimoLabel(),pet.getLimpiezaLabel(),this.coin,this.miniJuego,irTienda);
        this.center.getChildren().addAll(pet.getImageView());
        VBox sostenFood = new VBox(almacen.getRoot(),this.alimentar);
        sostenFood.setAlignment(Pos.TOP_RIGHT);
        sostenFood.setSpacing(5);
        this.right.getChildren().addAll(sostenFood);
        this.right.setSpacing(5);
        
        PetPane.root.setFocusTraversable(true);
        this.setUpClean();
//        PetPane.root.setCursor(new ImageCursor(this.cursorImage.getImage()));
        
       
        pet.verificando();
        time.iniciar();
        
    
    }
    
   
    
    
    public  BorderPane getRoot(){
    
        return this.root;
    }
    
    public void alimentar(){
    
        System.out.println("adfg");
        if(FeedBag.misComidas.size()!=0){
        
            Comida c = FeedBag.misComidas.get(0);
            int temp = Integer.parseInt(pet.getAlimentacionLabel().getText())+c.getCantidadAlim();
            pet.getAlimentacionLabel().setText(String.valueOf(temp));
            FeedBag.misComidas.remove(c);
            PetPane.almacen.getRoot().getChildren().remove(c.getComidaView());
            almacen.mostrarPrimero();
            
            
        }
        
        
    
    }
    
    public void setUpClean(){
    
         pet.getImageView().setOnMouseEntered(new EventHandler() {
            @Override
            public void handle(Event event) {
                PetPane.root.setCursor(new ImageCursor(cursorImage.getImage()));
            }
        });
        
        pet.getImageView().setOnMouseExited(new EventHandler() {
            @Override
            public void handle(Event event) {
               PetPane.root.setCursor(Cursor.DEFAULT);
            }
        });
        
        pet.getImageView().addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
             @Override
             public void handle(MouseEvent event) {
                 pet.getLimpiezaLabel().setText(String.valueOf(Integer.parseInt(pet.getLimpiezaLabel().getText())+1) );
             }
         });
        
        
    }
    
    public void irTienda(){
    
        Tienda tienda = new Tienda();
        tienda.posicionarComida();
        PetGame.ventana.getScene().setRoot(tienda.getRoot());
    }
    
    public void jugar(){
    
        PetGame.ventana.setScene(new Scene(new GamePane().getRoot(),300,250));
        PetGame.ventana.show();
        
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package petgame;

import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.scene.control.Label;

/**
 *
 * @author Israel
 */
public class Vida implements Runnable{
    private Label vidaLabel;
    private int cont;
    
    public Vida(){
    
        this.vidaLabel = new Label("500");
        this.cont = 0;
    }

    @Override
    public void run() {
        while(true){
            try {
                    cont++;
                    String nuevoValor = String.valueOf(Integer.parseInt(vidaLabel.getText())-cont);
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {

                            if(cont == 360000){
                                if(Integer.parseInt(vidaLabel.getText())>0 ){
                                    vidaLabel.setText(nuevoValor);

                                }
                            }

                        }
                    });
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Vida.class.getName()).log(Level.SEVERE, null, ex);
                }
        }
    }

    public Label getVidaLabel() {
        return vidaLabel;
    }

    public void setVidaLabel(Label vidaLabel) {
        this.vidaLabel = vidaLabel;
    }
    
    
    
    
    public void iniciarHilo(){
    
        Thread hiloVida = new Thread(this);
        hiloVida.setDaemon(true);
        hiloVida.start();
        
        
    }
    
    
}

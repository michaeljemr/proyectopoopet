/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package petgame;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

/**
 *
 * @author Israel
 */
public class Arco {
    
  
    private ImageView arcoImageView = new ImageView();
    
    
    public Arco(){
    
        this.arcoImageView.setImage(new Image(this.getClass().getResourceAsStream("/Images/arco.png")));
        this.arcoImageView.setFitHeight(50);
        this.arcoImageView.setFitWidth(50);
        this.arcoImageView.rotateProperty().set(45);
        this.arcoImageView.setX(100);
        this.arcoImageView.setY(100);
       
        
    
    }

    public ImageView getArcoImageView() {
        return arcoImageView;
    }

    public void setArcoImageView(ImageView arcoImageView) {
        this.arcoImageView = arcoImageView;
    }

   
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poopet;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

/**
 *
 * @author Gabriel
 */
public class Bow {
    
    private ImageView arcoImageView = new ImageView();
    
    public Bow(){
    
        this.arcoImageView.setImage(new Image(this.getClass().getResourceAsStream("/Images/arco.png")));
        this.arcoImageView.setFitHeight(25);
        this.arcoImageView.setFitWidth(25);
        this.arcoImageView.rotateProperty().set(45);
        this.arcoImageView.setX(250/2);
        this.arcoImageView.setY(260);
  
    }
    public ImageView getArcoImageView() {
        return arcoImageView;
    }

    public void setArcoImageView(ImageView arcoImageView) {
        this.arcoImageView = arcoImageView;
    }
    
}

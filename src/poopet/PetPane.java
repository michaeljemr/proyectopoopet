/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poopet;

import com.google.gson.Gson;
import java.awt.MouseInfo;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.ImageCursor;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.Clipboard;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import poopet.GsonUtils.DataBag;
import poopet.GsonUtils.DataPet;
import poopet.GsonUtils.DataTimer;
import poopet.GsonUtils.JsonFileIO;
import poopet.comida.Comida;
import poopet.comida.Tienda;

/**
 *
 * @author Gabriel
 */
public class PetPane {
    public static  BorderPane root;
    public static Pane center;
    private HBox top;
    private VBox right;
    private VBox left;
    private Button miniJuego;
    private Pet pet;
    public static Label coin;
    private Label tiempo;
    private Button alimentar;
    private ImageView cursorImage;
    public static Bag almacen;
    public ImageView fondo;
    
    
    public PetPane(){
    
        
        pet = new Pet();
        tiempo = new Label("0");
        Time time = new Time(tiempo);
        pet.petMovement();
        pet.verificando();
        time.iniciar();
        almacen = new Bag();
        almacen.mostrarPrimero();
        
        setUpPetPane();
        
    
    }
    
     public PetPane(DataPet dp,DataTimer dt,ArrayList<DataBag> ad){
    
        pet = new Pet(dp);
        tiempo = new Label(String.valueOf(dt.getTime()));
        Time time = new Time(tiempo);
        pet.petMovement();
        pet.verificando();
        time.iniciar();
        almacen = new Bag();
        almacen.llenarMisComidas(ad);
        almacen.mostrarPrimero();
        setUpPetPane();
    }
     
      public void setUpPetPane(){
    
        this.cursorImage = new ImageView(new Image("Images/PANTALLA PRINCIPAL.PNG"));
        this.cursorImage.setFitHeight(10);
        this.cursorImage.setFitWidth(10);
        
        
        center = new Pane();
        this.root = new BorderPane();
        this.top = new HBox();
        top.setSpacing(30);
        this.right = new VBox();
        this.left = new VBox();
        left.setSpacing(10);
        
        Button irTienda = new Button("Tienda");
        Button salir = new Button ("salir");
        this.miniJuego = new Button("Minijuego");
        this.alimentar = new Button("Alimentar");
        
        this.miniJuego.setOnAction(e->this.jugar());
        irTienda.setOnAction(e->this.irTienda());
        this.alimentar.setOnAction(e->this.alimentar());
        salir.setOnAction(e->this.salir());
       
        coin = new Label();
        this.coin.setText(String.valueOf(pet.getDinero()));
        
        this.root.setTop(this.top);
        this.root.setRight(this.right);
        this.root.setLeft(this.left);
        this.root.setCenter(center);
        
        HBox alimensoport = new HBox();
        alimensoport.getChildren().addAll(new Label("Alimentacion: "),pet.getAlimentacionLabel());
        alimensoport.setSpacing(5);
        
        HBox animoSoport = new HBox();
        animoSoport.getChildren().addAll(new Label("Animo: "),pet.getAnimoLabel());
        animoSoport.setSpacing(5);
        
        HBox limpiezaSoport = new HBox();
        limpiezaSoport.getChildren().addAll(new Label("Limpieza: "),pet.getLimpiezaLabel());
        limpiezaSoport.setSpacing(5);
        
        HBox coinSoport = new HBox();
        coinSoport.getChildren().addAll(new Label("Coins: "),PetPane.coin);
        coinSoport.setSpacing(5);
        
        HBox lifeSoport = new HBox();
        lifeSoport.getChildren().addAll(new Label("Vida: "),pet.getLife().getLifeLabel());
        lifeSoport.setSpacing(5);
        
        
        HBox timeSoport = new HBox();
        timeSoport.getChildren().addAll(new Label("Tiempo: "),tiempo);
        timeSoport.setSpacing(5);
        
        
        this.top.getChildren().addAll(lifeSoport,timeSoport);
        this.left.getChildren().addAll(alimensoport,animoSoport,limpiezaSoport,coinSoport,this.miniJuego,irTienda,salir);
        this.center.getChildren().addAll(pet.getImageView());
        VBox sostenFood = new VBox(almacen.getRoot(),this.alimentar);
        sostenFood.setAlignment(Pos.TOP_RIGHT);
        sostenFood.setSpacing(5);
        this.right.getChildren().addAll(sostenFood);
        this.right.setSpacing(5);
        
        PetPane.root.setFocusTraversable(true);
        this.setUpClean();
      
    }
      
      
        public  BorderPane getRoot(){
    
        return this.root;
    }
        
        
          public void alimentar(){
    
        System.out.println("adfg");
        if(Bag.misComidas.size()!=0){
        
            Comida c = Bag.misComidas.get(0);
            int temp = Integer.parseInt(pet.getAlimentacionLabel().getText())+c.getCantidadAlim();
            pet.getAlimentacionLabel().setText(String.valueOf(temp));
            Bag.misComidas.remove(c);
            PetPane.almacen.getRoot().getChildren().remove(c.getComidaView());
            almacen.mostrarPrimero();
            
            
        }
        
        
    
    }
          
          
           public void setUpClean(){
    
         pet.getImageView().setOnMouseEntered(new EventHandler() {
            @Override
            public void handle(Event event) {
                PetPane.root.setCursor(new ImageCursor(cursorImage.getImage()));
            }
        });
        
        pet.getImageView().setOnMouseExited(new EventHandler() {
            @Override
            public void handle(Event event) {
               PetPane.root.setCursor(Cursor.DEFAULT);
            }
        });
        
        pet.getImageView().addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
             @Override
             public void handle(MouseEvent event) {
                 pet.getLimpiezaLabel().setText(String.valueOf(Integer.parseInt(pet.getLimpiezaLabel().getText())+1) );
             }
         });
        
        
    }
           
           
           public void irTienda(){
    
        
        Tienda tienda = new Tienda();
        tienda.posicionarComida();
        PooPet.ventana.getScene().setRoot(tienda.getRoot());
    }
           
           public void jugar(){
    
        
        PooPet.ventana.getScene().setRoot(new GamePane().getRoot());
        
    }
           
           
           public void salir(){
        
        try {
                JsonFileIO.serializar(this.pet.petToData());
                JsonFileIO.serializar(new DataTimer(Integer.parseInt(this.tiempo.getText())));
                JsonFileIO.ArraySerial(PetPane.almacen.feedbagToData());
        } catch (IOException ex) {
            Logger.getLogger(PetPane.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.exit(0);
    
    }
           
           
           
           
     
     
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poopet;

import java.awt.MouseInfo;
import java.util.ArrayList;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import poopet.minijuego.Cofre;

/**
 *
 * @author Gabriel
 */
public class GamePane {
    
    private BorderPane root ;
    public static  Pane center;
    private Button arcoNode;
    private Bow arco = new Bow();
    private ArrayList<Cofre> cofres ;
     public GamePane(){
         cofres = new ArrayList<>();
         center = new Pane();
         this.root = new BorderPane();
         this.arcoNode = new Button();
         this.arcoNode.setGraphic(arco.getArcoImageView());
         arcoNode.setLayoutX(250/2);
         arcoNode.setLayoutY(220);
         center.getChildren().add(arcoNode);
         this.root.setOnMouseMoved(e->this.rotateActionArch());
         this.root.setCenter(center);
         inicializarCofres();
     }
    
      public  void rotateActionArch(){
    
        this.arcoNode.setRotate(Math.toDegrees(Math.cos(MouseInfo.getPointerInfo().getLocation().x)));
    } 
      
    public void inicializarCofres(){
    
        
        for(int i = 0 ; i<5 ;i++ ){
            System.out.println("Se inicializan los cofres");
            Cofre temp = new Cofre();
            this.cofres.add(temp);
            GamePane.center.getChildren().add(temp.getCofreView());
        }
        
    }
    
     public BorderPane getRoot(){
        return this.root;
    }
      
      
}

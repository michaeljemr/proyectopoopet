/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poopet.minijuego;

import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import poopet.GamePane;

/**
 *
 * @author Gabriel
 */
public class Sorpresa implements Runnable {
    
    private ImageView sorpresaView;
    
    public Sorpresa( String stringPath){
    
        sorpresaView = new ImageView(new Image(stringPath));
        sorpresaView.setFitHeight(30);
        sorpresaView.setFitWidth(30);
    }
    
     public ImageView getSorpresaView() {
        return sorpresaView;
    }
     
    public void setSorpresaView(ImageView sorpresaView) {
        this.sorpresaView = sorpresaView;
    }
    
        @Override
    public void run() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(Sorpresa.class.getName()).log(Level.SEVERE, null, ex);
        }
        Platform.runLater(new Runnable(){
            @Override
            public void run() {
                GamePane.center.getChildren().remove(sorpresaView);
            }
        });
    }
    
    public void inicialr(){
    
        Thread hilo = new Thread(this);
        hilo.setDaemon(true);
        hilo.start();
    }
    
    
    
    

    
    
    
}

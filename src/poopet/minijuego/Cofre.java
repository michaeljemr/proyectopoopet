/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poopet.minijuego;

import java.awt.Cursor;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import poopet.GamePane;
import poopet.Pet;
import poopet.PooPet;
import poopet.PetPane;

/**
 *
 * @author Gabriel
 */
public class Cofre {
    
    private Sorpresa sorpresa;
    private ImageView cofreView;
    private int cofreType;
    
    
     public Cofre(){
    
        cofreType = 0;
        cofreView = new ImageView(new Image("Images/cofre.png"));
        cofreView.setFitHeight(30);
        cofreView.setFitWidth(30);
        posicionarCofre();
        llenarCofre();
        abrirCofre();
        
    }
     
     public void posicionarCofre(){
    
        cofreView.setX(new Random().nextInt(240));
        cofreView.setY(new Random().nextInt(160));
    
    }
    
    
    public void  llenarCofre(){
        generarNuevaSorpresa();
        sorpresa.getSorpresaView().setX(cofreView.getX());
        sorpresa.getSorpresaView().setY(cofreView.getY());
    }
    
    public void abrirCofre(){
    
         cofreView.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
             @Override
             public void handle(MouseEvent event) {
                 GamePane.center.getChildren().add(sorpresa.getSorpresaView());
                 System.out.println("cofreType: " + cofreType);
                 
                 if(cofreType == 2 ){
                     
                     
                 
                     Pet.animoLabel.setText(String.valueOf(Integer.parseInt(Pet.animoLabel.getText())+2));
                     PooPet.ventana.setScene(new Scene(PetPane.root));
                 }
                 else if(cofreType == 1){
                 
                     Pet.dinero += 1;
                 }
                 sorpresa.inicialr();
                 posicionarCofre();
                 llenarCofre();
                 GamePane.center.getChildren().remove(cofreView);
                 GamePane.center.getChildren().add(cofreView);
                 System.out.println("Se dio click en el cofre");
             }
         });
    }
    
     public void generarNuevaSorpresa(){
        
        Random rand = new Random();
        if( rand.nextInt(100) < 75){
        
            sorpresa = new Sorpresa("Images/coin.png");
            cofreType = 1;
            System.out.println("Se creo coin");
        }
        else{
        
            
            sorpresa = new Sorpresa("Images/bomb.png");
            cofreType = 2;
            System.out.println("Se creo bomb");
        }
        
    }
     
     public Sorpresa getSorpresa() {
        return sorpresa;
    }

    public void setSorpresa(Sorpresa sorpresa) {
        this.sorpresa = sorpresa;
    }

    public ImageView getCofreView() {
        return cofreView;
    }

    public void setCofreView(ImageView cofreView) {
        this.cofreView = cofreView;
    }
     
     
    
    
    
    
    
     
     
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poopet;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.StageStyle;
import poopet.GsonUtils.DataBag;
import poopet.GsonUtils.DataPet;
import poopet.GsonUtils.DataTimer;
import poopet.GsonUtils.JsonFileIO;

/**
 *
 * @author Gabriel
 */
public class PrincipalPane {
    
     private Pane root;
    private Button jugar;
    
    
     public PrincipalPane(){
          this.root = new Pane();
          Label petgame = new Label("POOPET");
          petgame.setFont(new Font("cambria",32));
          petgame.setTextFill(Color.BLUEVIOLET);
          petgame.setLayoutX((250/2)-50);
          petgame.setLayoutY(300/3);
          petgame.setAlignment(Pos.CENTER);
          root.setStyle("-fx-background-color: aquamarine");
          this.jugar = new Button("Jugar");
          this.jugar.setLayoutX(250/2);
          this.jugar.setLayoutY(300/2);
          this.root.getChildren().addAll(this.jugar,petgame);
          this.jugar.setOnAction(e->this.iniciarJuego());
    }
     
     public Pane getRoot() {
        return root;
    }

    public void setRoot(Pane root) {
        this.root = root;
    }

    public Button getJugar() {
        return jugar;
    }

    public void setJugar(Button jugar) {
        this.jugar = jugar;
    }
    
    
     public void iniciarJuego(){
        System.out.println("se inicia el juego");
        ArrayList<DataBag> dB = null;
        DataTimer dT = null;
        DataPet datapet = null;
        try {
            datapet = (DataPet)JsonFileIO.deserializar("PooPet.json");
            dT = (DataTimer)JsonFileIO.deserializar("timer.json");
            dB = JsonFileIO.ArrayDeserial("feedbag.json");
            System.out.println("datapet:"+datapet.getNombre());
            System.out.println("dT"+dT.getTime());
            if(!dB.isEmpty()){
                System.out.println("galleta:"+dB.get(0).getName());
            }
            if(!datapet.isVive() || datapet==null || dT == null || dB==null ){
                this.pedirDatosPet();
                PooPet.ventana.setScene(new Scene(new PetPane().getRoot(),300,250));
                PooPet.ventana.show();
            }else{
                PooPet.ventana.setScene(new Scene(new PetPane(datapet,dT,dB).getRoot(),300,250));
                PooPet.ventana.show();
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(PrincipalPane.class.getName()).log(Level.SEVERE, null, ex);
        }
      
        
    }
     
     
      public void pedirDatosPet(){
    
         // CODE FOR USER NAME
        TextInputDialog dialogo = new TextInputDialog();
        dialogo.setTitle("Datos de usuario");
        dialogo.setHeaderText("Ingrese su nombre ");
        dialogo.setContentText("Por Favor escriba su nombre");
        dialogo.initStyle(StageStyle.UTILITY);
        Optional<String> respuesta = dialogo.showAndWait();
        System.out.println(respuesta.get());
        
        // CODE FOR PET NAME
        
        TextInputDialog dialogo2 = new TextInputDialog();
        dialogo2.setTitle("Datos de la mascota");
        dialogo2.setHeaderText("Ingrese el nombre dela mascota");
        dialogo2.setContentText("Por Favor escriba el nombre");
        dialogo2.initStyle(StageStyle.UTILITY);
        Optional<String> respuesta2 = dialogo2.showAndWait();
        Pet.nombre = respuesta2.get();
        System.out.println(Pet.nombre);
        
    }
       
   
}

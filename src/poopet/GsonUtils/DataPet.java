/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poopet.GsonUtils;

/**
 *
 * @author Gabriel
 */
public class DataPet implements Data {
    
    private double coin;
    private String nombre;
    private int life;
    private int lifecont;
    private int animo;
    private int alimentacion;
    private int limpieza;
    private int cont;
    private boolean vive;
    
    
       public DataPet(double coin, String nombre, int life,int lifecont, int animo, int alimentacion, int limpieza, int cont, boolean vive) {
        this.coin = coin;
        this.nombre = nombre;
        this.life = life;
        this.animo = animo;
        this.alimentacion = alimentacion;
        this.limpieza = limpieza;
        this.cont = cont;
        this.vive = vive;
        this.lifecont = lifecont;
    }
       
        public int getLifecont() {
        return lifecont;
    }

    public void setLifecont(int lifecont) {
        this.lifecont = lifecont;
    }
    
    

    public double getCoin() {
        return coin;
    }

    public void setCoin(double coin) {
        this.coin = coin;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getLife() {
        return life;
    }

    public void setLife(int life) {
        this.life = life;
    }

    public int getAnimo() {
        return animo;
    }

    public void setAnimo(int animo) {
        this.animo = animo;
    }

    public int getAlimentacion() {
        return alimentacion;
    }

    public void setAlimentacion(int alimentacion) {
        this.alimentacion = alimentacion;
    }

    public int getLimpieza() {
        return limpieza;
    }

    public void setLimpieza(int limpieza) {
        this.limpieza = limpieza;
    }

    public int getCont() {
        return cont;
    }

    public void setCont(int cont) {
        this.cont = cont;
    }

    public boolean isVive() {
        return vive;
    }

    public void setVive(boolean vive) {
        this.vive = vive;
    }

    @Override
    public String getFileName() {
        return "PooPet.json";
    }
       
       
     
    
    
    
    
    
}

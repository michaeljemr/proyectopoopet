/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poopet.GsonUtils;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Gabriel
 */
public class JsonFileIO {
    
     public static void serializar(Data data) throws IOException{
    
        Gson gson = new Gson();
        String json = gson.toJson(data);
        FileWriter fw = new FileWriter(data.getFileName());
        fw.write(json);
        fw.flush();
        fw.close();
        
    }
     
     public static Data deserializar(String path) throws FileNotFoundException{
    
        JsonParser parser = new JsonParser();
        FileReader fr = new FileReader(path);
        JsonElement datos = parser.parse(fr);
        Gson gson = new Gson();
        Data data = null;
        if(path.equalsIgnoreCase("PooPet.json")){
        
            data = gson.fromJson(datos, DataPet.class);
            
        }
        else if(path.equalsIgnoreCase("Timer.json")){
        
            data = gson.fromJson(datos, DataTimer.class);
            
        }
        
        
        return data;
    }
     
    public static void ArraySerial(ArrayList<DataBag> data) throws IOException{
    
        Gson gson = new Gson();
        String json = gson.toJson(data);
        FileWriter fw = new FileWriter("Bag.json");
        fw.write(json);
        fw.flush();
        fw.close();
        
    } 
    
    public static ArrayList<DataBag> ArrayDeserial(String path) throws FileNotFoundException{
    
    
        JsonParser parser = new JsonParser();
        FileReader fr = new FileReader(path);
        JsonElement datos = parser.parse(fr);
        Gson gson = new Gson();
        Type foundListType = new TypeToken<ArrayList<DataBag>>(){}.getType();
        ArrayList<DataBag> data  = gson.fromJson(datos, foundListType);
        
        
        return data;
    }
     
     
     
     
    
}

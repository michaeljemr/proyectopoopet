/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poopet;

import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.scene.control.Label;

/**
 *
 * @author Gabriel
 */
public class Life implements Runnable {
    private Label lifeLabel;
    private int cont;
    
     public Life(){
    
        this.lifeLabel = new Label("500");
        this.cont = 0;
    }
     
     public Life(Label vidaLabel, int cont) {
        this.lifeLabel = vidaLabel;
        this.cont = cont;
    }
     
     @Override
    public void run() {
        while(true){
            try {
                    cont++;
                     
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {

                            if(cont == 3600){
                                if(Integer.parseInt(lifeLabel.getText())>0 ){
                                    
                                    lifeLabel.setText(String.valueOf(Integer.parseInt(lifeLabel.getText())-1));
                                    System.out.println(cont);

                                }
                                cont=0;
                            }

                        }
                    });
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Life.class.getName()).log(Level.SEVERE, null, ex);
                }
        }
    }
    
     public Label getLifeLabel() {
        return lifeLabel;
    }

    public void setLifeLabel(Label lifeLabel) {
        this.lifeLabel = lifeLabel;
    }

     public void iniciarHilo(){
    
        Thread hiloVida = new Thread(this);
        hiloVida.setDaemon(true);
        hiloVida.start();
        
        
    }

    public int getCont() {
        return cont;
    }

    public void setCont(int cont) {
        this.cont = cont;
    }
    
     
     
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poopet;

import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.scene.control.Label;

/**
 *
 * @author Gabriel
 */
public class Status implements Runnable {
    
    private Label statusLabel;
    private int cont;
    
    public Status(Label statusLabel){
        this.statusLabel= statusLabel;
    }
    
    public Label getStatusLabel(){
        return statusLabel;
    }
  
    public void setStatusLabel(Label statusLabel){
        this.statusLabel= statusLabel;
    }

    @Override
    public void run() {
        while(true){
            try{
            cont++;
            Platform.runLater(new Runnable(){
                @Override
                public void run(){
                    if(Integer.parseInt(statusLabel.getText())>10){
                        statusLabel.setText(String.valueOf(10));
                    }
                    if(cont==60){
                        if(Integer.parseInt(statusLabel.getText())>0){
                            statusLabel.setText(String.valueOf(Integer.parseInt(statusLabel.getText())-1));
                        }
                        cont=0;
                    }
                }
            });
            
            Thread.sleep(1000);
        } catch(InterruptedException ex){
                Logger.getLogger(Time.class.getName()).log(Level.SEVERE, null, ex);
        }
    }  
}
    public void iniciar(){
    
        Thread hiloActualizar = new Thread(this);
        hiloActualizar.setDaemon(true);
        hiloActualizar.start();
        ;
    }
}

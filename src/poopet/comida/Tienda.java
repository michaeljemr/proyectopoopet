/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poopet.comida;

import java.util.ArrayList;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import poopet.Bag;
import poopet.Pet;
import poopet.PooPet;
import poopet.PetPane;

/**
 *
 * @author Gabriel
 */
public class Tienda {
    
    private ArrayList<Comida> comidas;
    private VBox root = new VBox();
    private Button irAPet;
    private Label nombreComida;
    private Button comprar;
    private Comida deVenta;
    
    
    
    
     public Tienda(){
    
        root.setAlignment(Pos.CENTER);
        this.nombreComida = new Label("No Seleccionada");
        this.comidas = new ArrayList<>();
        this.comidas.add(new Comida(5,2,"galleta.png"));
        this.comidas.add(new Comida(15,3,"lasagna.png"));
        this.comidas.add(new Comida(100,10,"pizza.png"));
        this.comidas.add(new Comida(50,7,"pollo.png"));
        this.comidas.add(new Comida(30,5,"tuna.png"));
        
        irAPet = new Button("Ir A Pet");
        irAPet.setOnAction(e->this.irAPet());
        this.comprar = new Button("comprar");
        comprar.setOnAction(e->this.comprar());
        
        
    }
     
     
      public void posicionarComida(){
    
        for(Comida c:this.comidas){
            
            c.getComidaView().addEventHandler(MouseEvent.MOUSE_CLICKED,new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                   nombreComida.setText(c.getName());
                   deVenta = new Comida(c);
                   event.consume();
                }
            });
            
            HBox hijo = new HBox();
            hijo.getChildren().addAll(new Label(c.getName().toUpperCase()),c.getComidaView(),new Label(String.valueOf(c.getPrecio())));
            hijo.setSpacing(10);
            hijo.setAlignment(Pos.CENTER);
            
            this.root.getChildren().addAll(hijo);
            
        }
        HBox bottom  =  new HBox(this.comprar,this.nombreComida);
        bottom.setSpacing(10);
        bottom.setAlignment(Pos.CENTER);
        this.root.getChildren().addAll(bottom,irAPet);
        this.root.setSpacing(5);
    }
      
       public void irAPet(){
    
        PooPet.ventana.getScene().setRoot(PetPane.root);
    }
       
        public void comprar(){
    
        if(deVenta != null){
            
            if(Pet.dinero !=0 ){
                if(Pet.dinero >= deVenta.getPrecio()){
                    Bag.misComidas.add(new Comida(deVenta));
                    Pet.dinero = Pet.dinero-deVenta.getPrecio();
                    PetPane.almacen.mostrarPrimero();
                    System.out.println("Se vendio: "+deVenta.getName());
                }
            }
        }
    }
        
        public ArrayList<Comida> getComidas() {
        return comidas;
    }

    public void setComidas(ArrayList<Comida> comidas) {
        this.comidas = comidas;
    }

    public VBox getRoot() {
        return root;
    }

    public void setRoot(VBox root) {
        this.root = root;
    }
       
       
       
      
      
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poopet;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import poopet.GsonUtils.DataPet;
import poopet.GsonUtils.JsonFileIO;


/**
 *
 * @author Gabriel
 */
public class Pet implements Runnable {
    
    private ImageView imageView;
    public static int dinero;
    public static String nombre ;
    private Life life;
    public static Label animoLabel;
    private Label alimentacionLabel;
    private Label limpiezaLabel;
    private int cont;
    public static  boolean vive;
    public static ArrayList<Poop> poops = new ArrayList<>();
    
    public Pet(){
    
        
        this.vive = true;
        this.cont=0;
        this.life = new Life();
        this.animoLabel = new Label("10");
        this.alimentacionLabel = new Label("10");
        this.limpiezaLabel = new Label("10");
        Status statusAnimo = new Status(this.animoLabel);
        Status statusAlimentacion = new Status(this.alimentacionLabel);
        Status statusLimpieza = new Status(this.limpiezaLabel);
      
        this.dinero = 100;
        
        this.imageView = new ImageView(new Image("Images/Koopa_(PMCS).gif"));
        this.imageView.setFitHeight(100);
        this.imageView.setFitWidth(100);
        this.imageView.setY(300/2);
        this.imageView.setX(250/2);
        
        this.life.iniciarHilo();
        statusAnimo.iniciar();
        statusAlimentacion.iniciar();
        statusLimpieza.iniciar();
    
    }
    
     public Pet(DataPet data){
    
        this.vive = data.isVive();
        this.cont = data.getCont();
        this.life = new Life(new Label(String.valueOf(data.getLife())),data.getLifecont());
        System.out.println("Vida de la mascota Json: "+ life.getLifeLabel().getText());
        
        animoLabel = new Label(String.valueOf(data.getAnimo()));
        alimentacionLabel = new Label(String.valueOf(data.getAlimentacion()));
        limpiezaLabel = new Label(String.valueOf(data.getLimpieza()));
        dinero = (int)data.getCoin();
        Status statusAnimo = new Status(this.animoLabel);
        Status statusAlimentacion = new Status(this.alimentacionLabel);
        Status statusLimpieza = new Status(this.limpiezaLabel);
        
        this.imageView = new ImageView(new Image("Images/Koopa_(PMCS).gif"));
        this.imageView.setFitHeight(100);
        this.imageView.setFitWidth(100);
        this.imageView.setY(300/2);
        this.imageView.setX(250/2);
        
        this.life.iniciarHilo();
        statusAnimo.iniciar();
        statusAlimentacion.iniciar();
        statusLimpieza.iniciar();
        
    }
     
     public ImageView getImageView() {
        return imageView;
    }
     
    public int getDinero() {
        return dinero;
    }
    
     public void setDinero(int dinero) {
        this.dinero = dinero;
    }
     
     public static String getNombre() {
        return nombre;
    }
     
    public static void setNombre(String nombre) {
        Pet.nombre = nombre;
    }
    
    public Life getLife() {
        return life;
    }
    
    public void setLife(Life life) {
        this.life = life;
    }
    
    public Label getAnimoLabel() {
        return animoLabel;
    }
    
    public static void setAnimoLabel(Label animoLabel) {
        Pet.animoLabel = animoLabel;
    }
    
    
    public  Label getAlimentacionLabel() {
        return alimentacionLabel;
    }
    
     public void setAlimentacionLabel(Label alimentacionLabel) {
        this.alimentacionLabel = alimentacionLabel;
    }
     
    public Label getLimpiezaLabel() {
        return limpiezaLabel;
    }
    
    public void setLimpiezaLabel(Label limpiezaLabel) {
        this.limpiezaLabel = limpiezaLabel;
    }
    
    public DataPet petToData(){
    
        DataPet data = new DataPet(Pet.dinero,
                       Pet.nombre,
                       Integer.parseInt(this.life.getLifeLabel().getText()),
                       this.life.getCont(),
                       Integer.parseInt(Pet.animoLabel.getText()),
                       Integer.parseInt(this.alimentacionLabel.getText()),
                       Integer.parseInt(this.limpiezaLabel.getText()),
                       this.cont,
                       this.vive);
        
        
        return data;
    
    }
    
    public void petMovement(){
   
      Timeline timeline = new Timeline();
       timeline.setCycleCount(Timeline.INDEFINITE);
       timeline.setAutoReverse(true);
       KeyValue kv = new KeyValue(this.imageView.xProperty(),new Random().nextInt(250));
       KeyValue kv2 = new KeyValue(this.imageView.yProperty(),130);
       KeyValue kv3 = new KeyValue(this.imageView.yProperty(),150);
       
       KeyFrame kf = new KeyFrame(Duration.millis(3000),kv);
       KeyFrame kf2 = new KeyFrame(Duration.millis(200),kv2);
       KeyFrame kf3 = new KeyFrame(Duration.millis(200),kv3);
       
       timeline.getKeyFrames().add(kf);
       timeline.getKeyFrames().add(kf2);
       timeline.getKeyFrames().add(kf3);
       timeline.play();
   }
    
    
       public void verificarStatus(){
        ArrayList<String> status = new ArrayList<>();
        status.add(this.alimentacionLabel.getText());
        status.add(this.animoLabel.getText());
        status.add(this.limpiezaLabel.getText());
        int cont=0;
        for(int i=0;i<status.size();i++){
        
            if(status.get(i).equals("0")){
            
                cont++;
            }
            
        
        }
        if(cont>0){
            
        
                this.life.getLifeLabel().setText(String.valueOf(Integer.parseInt(this.life.getLifeLabel().getText())-(100*cont)));
            
        }
       }
        
        @Override
        public void run() {
         while(vive){
            try {
                cont++;
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        
                        
                       verificarStatus();
                       if(Integer.parseInt(life.getLifeLabel().getText())<=0 ){
                       
                           vive = false;
                           Pane paneC = new Pane();
                           paneC.setStyle("-fx-background-color: black");
                           Label gameover = new Label("GAME OVER");
                           gameover.setLayoutX(250/2);
                           gameover.setLayoutY(300/2);
                           gameover.setTextFill(Color.YELLOW);
                           paneC.getChildren().add(gameover);
                           PooPet.ventana.getScene().setRoot(paneC);
                           try {
                               JsonFileIO.serializar(petToData());
                           } catch (IOException ex) {
                               Logger.getLogger(Pet.class.getName()).log(Level.SEVERE, null, ex);
                           }
                       }
                       if(dinero != 100){
                            if(dinero < 0){
                                dinero = 0;
                            }
                            PetPane.coin.setText(String.valueOf(dinero));
                           
                       }
                       
                        
                       
                       if(poops.isEmpty() || poops.size()<10){
                       
                                Poop poop = new Poop();
                                poops.add(poop);
                                poop.iniciar();
                       }
                       
                       for(Poop p:poops){
                       
                           if(p.isIsOnPane() == true){
                           
                               PetPane.center.getChildren().remove(p);
                               break;
                           }
                       }
                       
                    }
                });
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(Time.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
         try {
                               Thread.sleep(3000);
                           } catch (InterruptedException ex) {
                               Logger.getLogger(Pet.class.getName()).log(Level.SEVERE, null, ex);
                           }
                           System.exit(0);
        
    }
    
    public void verificando(){
    
        Thread hiloVerificar = new Thread(this);
        hiloVerificar.setDaemon(true);
        hiloVerificar.start();
    
    }
    
    } 
    

    
     
     
    
    
    
    
     
     
     
    
    
    
    
    


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poopet;

import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.scene.control.Label;
import poopet.GsonUtils.DataTimer;

/**
 *
 * @author Gabriel
 */
public class Time implements Runnable {
    private Label tiempoView;
    private String segundo;
    
    public Time(Label tiempoView){
       this.tiempoView = tiempoView;
    }
    
     public Label getTiempoView() {
        return tiempoView;
    }

    public void setTiempoView(Label tiempoView) {
        this.tiempoView = tiempoView;
    }

    public String getSegundo() {
        return segundo;
    }

    public void setSegundo(String segundo) {
        this.segundo = segundo;
    }
    
    
    public DataTimer tiempoToData(){
    
        DataTimer data = new DataTimer(Integer.parseInt(this.tiempoView.getText()));
    
        return null;
    }
    @Override
    public void run() {
       
        while(true){
            try {
                int cont = Integer.parseInt(tiempoView.getText());
                
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        tiempoView.setText(String.valueOf(cont+1));
                        
                    }
                });
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(Time.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    public void iniciar(){
    
        Thread hiloTiempo = new Thread(this);
        hiloTiempo.setDaemon(true);
        hiloTiempo.start();
        ;
    }
}

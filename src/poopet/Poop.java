/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poopet;


import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.ImageCursor;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

/**
 *
 * @author Gabriel
 */
public class Poop implements Runnable {
    
    private ImageView poopView;
    private int rand ;
    private boolean isOnPane;
    
    public Poop(){
    
        this.poopView = new ImageView(new Image("Images/poop.png"));
        this.rand = new Random().nextInt(300-60)+60;
        
        System.out.println(rand);
        poopView.setX(new Random().nextInt(230));
        poopView.setY(210);
        poopView.setFitHeight(20);
        poopView.setFitWidth(20);
        this.isOnPane = false;
        this.setUpClean();
        
        
    }
    
     public void setUpClean(){
    
         poopView.setOnMouseEntered(new EventHandler() {
            @Override
            public void handle(Event event) {
                PetPane.root.setCursor(new ImageCursor(new Image("Images/PANTALLA PRINCIPAL.PNG")));
            }
        });
        
        poopView.setOnMouseExited(new EventHandler() {
            @Override
            public void handle(Event event) {
               PetPane.root.setCursor(Cursor.DEFAULT);
            }
        });
        
        poopView.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
             @Override
             public void handle(MouseEvent event) {
                 PetPane.center.getChildren().remove(poopView);
                 Pet.poops.remove(this);
                 System.out.println(Pet.poops.size()+"click");
             }
         });
        
        
    }
     
    public void run() {
            
        
            try {
                Thread.sleep(rand*1000);
             
            } catch (InterruptedException ex) {
                Logger.getLogger(Poop.class.getName()).log(Level.SEVERE, null, ex);
            }
        
            
            Platform.runLater(new Runnable(){
                @Override
                public void run() {
                    
                    PetPane.center.getChildren().addAll(poopView);
                    
                }
            });
               
            
            
          isOnPane = true;
            
        
    }
    
     public ImageView getPoopView() {
        return poopView;
    }

    public void setPoopView(ImageView poopView) {
        this.poopView = poopView;
    }

    public int getRand() {
        return rand;
    }

    public void setRand(int rand) {
        this.rand = rand;
    }

    public boolean isIsOnPane() {
        return isOnPane;
    }

    public void setIsOnPane(boolean isOnPane) {
        this.isOnPane = isOnPane;
    }
    
    
     public void iniciar(){
    
        Thread hiloPoop = new Thread(this);
        hiloPoop.setDaemon(true);
        hiloPoop.start();
        
    
    }

    
}
